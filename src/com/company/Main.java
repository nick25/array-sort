

package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        List<Double> numbers = new ArrayList();


        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            BufferedWriter bw = new BufferedWriter(new FileWriter("Numbers.txt"));


                 String line;
                 while(!(line = br.readLine()).equals("")){
                     double number = Double.parseDouble(line);
                     if (number %2 == 0){
                         number *= 0.1;
                      }
                       numbers.add(number);
                 }

                  numbers.sort(new Comparator<Double>() {
                        public int compare(Double o1,Double o2) {
                            if (o1 < o2) {
                                return 1;
                            } else if (o1.equals(o2)) {
                                return 0;
                            }else{
                                return -1;
                            }

                        }
                    });

                    int i;
                    for(i = 0; i < numbers.size(); ++i) {
                        bw.write((numbers.get(i) +"\n"));
                        bw.flush();
                    }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

